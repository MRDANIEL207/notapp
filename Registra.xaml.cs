﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace Notas
{
    public partial class Registra : Window
    {
        private string miConexion = ConfigurationManager.ConnectionStrings["Notas.Properties.Settings.GestionNotasConnectionString"].ConnectionString;

        public Registra()
        {
            InitializeComponent();
        }

        private void Registrarse_Click(object sender, RoutedEventArgs e)
        {
            string usuario = txtNomUsuario.Text.Trim();
            string nombre = txtNombre.Text.Trim();
            string apellidoPaterno = txtApellidoPaterno.Text.Trim();
            string apellidoMaterno = txtApellidoMaterno.Text.Trim();
            string correoElectronico = txtCorreoElectronico.Text.Trim();
            string contraseña = txtContraseña.Text.Trim();

            if (string.IsNullOrEmpty(usuario) || string.IsNullOrEmpty(nombre) || string.IsNullOrEmpty(apellidoPaterno) || string.IsNullOrEmpty(apellidoMaterno) || string.IsNullOrEmpty(correoElectronico) || string.IsNullOrEmpty(contraseña))
            {
                MessageBox.Show("Todos los campos son obligatorios. Por favor, llene todos los campos.");
                return;
            }

            // Validar formato de correo electrónico utilizando expresión regular
            if (!EsCorreoElectronicoValido(correoElectronico))
            {
                MessageBox.Show("El formato del correo electrónico no es válido.");
                return;
            }

            string query = "INSERT INTO usuario (Usuario, Nombre, Apellido_paterno, Apellido_materno, correo_electronico, Contraseña) VALUES (@Usuario, @Nombre, @ApellidoPaterno, @ApellidoMaterno, @CorreoElectronico, @Contraseña)";

            using (SqlConnection connection = new SqlConnection(miConexion))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Usuario", usuario);
                command.Parameters.AddWithValue("@Nombre", nombre);
                command.Parameters.AddWithValue("@ApellidoPaterno", apellidoPaterno);
                command.Parameters.AddWithValue("@ApellidoMaterno", apellidoMaterno);
                command.Parameters.AddWithValue("@CorreoElectronico", correoElectronico);
                command.Parameters.AddWithValue("@Contraseña", contraseña);

                try
                {
                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Registro exitoso.");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Error al registrar usuario.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al intentar registrar usuario: " + ex.Message);
                }
            }
        }

        private bool EsCorreoElectronicoValido(string correoElectronico)
        {

            string patron = @"^[\w-]+(\.[\w-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$";
            Regex regex = new Regex(patron);
            return regex.IsMatch(correoElectronico);
        }

        private void Cancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System.Windows;
using System.Windows.Controls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System;
using System.Windows.Media.Imaging;

namespace Notas
{
    public partial class MainWindow : Window
    {
        private bool ContraseñaVisible = false;
        private string contraseña = "";
        private TextBox plainTextBox;
        private string miConexion = ConfigurationManager.ConnectionStrings["Notas.Properties.Settings.GestionNotasConnectionString"].ConnectionString;

        public MainWindow()
        {
            InitializeComponent();
        }

        private bool primerClick = true;

        private void MostrarContraseña_Click(object sender, RoutedEventArgs e)
        {

            if (ContraseñaVisible)
            {
                txtContraseña.Password = contraseña;
                ((DockPanel)txtContraseña.Parent).Children.Remove(plainTextBox);
                txtContraseña.Visibility = Visibility.Visible;
            }
            else 
            {

                contraseña = txtContraseña.Password;

                txtContraseña.Visibility = Visibility.Collapsed;

                plainTextBox = new TextBox
                {
                    Text = contraseña,
                    Width = txtContraseña.Width,
                    Height = txtContraseña.Height,
                    Margin = txtContraseña.Margin,
                    Background = txtContraseña.Background,
                    VerticalAlignment = VerticalAlignment.Center
                };

                DockPanel.SetDock(plainTextBox, Dock.Right);
                ((DockPanel)txtContraseña.Parent).Children.Add(plainTextBox);
            }

            Image image = new Image();
            if (!primerClick) 
            {
                image.Source = new BitmapImage(new Uri(ContraseñaVisible ? @"C:\Users\mrdan\OneDrive\Documentos\Programacion .NET\Notas\OjoAbierto.png" : @"C:\Users\mrdan\OneDrive\Documentos\Programacion .NET\Notas\OJO.jpg"));
            }
            else 
            {
                image.Source = new BitmapImage(new Uri(@"C:\Users\mrdan\OneDrive\Documentos\Programacion .NET\Notas\OJO.jpg"));
                primerClick = false;
            }
            btnContraseña.Content = image;

            ContraseñaVisible = !ContraseñaVisible;
        }




        private void Ingresar_Click(object sender, RoutedEventArgs e)
        {
            string usuario = txtUsuario.Text.Trim();
            string contraseña = txtContraseña.Password.Trim();

            string query = "SELECT COUNT(*) FROM usuario WHERE Usuario = @Usuario AND Contraseña = @Contraseña";

            using (SqlConnection connection = new SqlConnection(miConexion))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Usuario", usuario);
                command.Parameters.AddWithValue("@Contraseña", contraseña);

                try
                {
                    connection.Open();
                    int count = (int)command.ExecuteScalar();

                    if (count > 0)
                    {
                        MessageBox.Show("Inicio de sesión exitoso.");
                    }
                    else
                    {
                        MessageBox.Show("Usuario o contraseña incorrectos.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al intentar iniciar sesión: " + ex.Message);
                }
            }
        }

        private void Registrarse_Click(object sender, RoutedEventArgs e)
        {

            Registra ventanaRegistro = new Registra();

            ventanaRegistro.Show();
        }

    }
}

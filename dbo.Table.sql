﻿CREATE TABLE [dbo].[cliente] (    idUsuario     INT           IDENTITY (1, 1) NOT NULL,
    [Usuario] NVARCHAR (50) NULL,
    [Contraseña]     NVARCHAR (50) NULL,
    [Nombre]     NVARCHAR (50) NULL,
    [Apellido_paterno]        NVARCHAR (50) NULL,
    [Apellido_materno]      NVARCHAR (50) NULL,
    [correo_electronico]          NVARCHAR (50)           NULL,
    PRIMARY KEY CLUSTERED (idUsuario ASC));

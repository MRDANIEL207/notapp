USE notas;

CREATE TABLE usuarios(
	idUsuario INT NOT NULL,
	nombre NVARCHAR(50) NOT NULL,
	contrasena NVARCHAR(20) NOT NULL,
	PRIMARY KEY CLUSTERED (idUsuario ASC)
);

CREATE TABLE categorias(
	idCategoria INT NOT NULL,
	nombreCategoria NVARCHAR(50) NOT NULL,
	idUsuario INT NOT NULL,
	PRIMARY KEY CLUSTERED (idCategoria ASC),
	CONSTRAINT [FK_categorias_ToTable] FOREIGN KEY (idUsuario) REFERENCES usuarios (idUsuario)
);

CREATE TABLE notas(
	idNota INT NOT NULL,
	titulo NVARCHAR(100) NOT NULL,
	contenido NTEXT NULL,
	idCategoria INT NOT NULL,
	PRIMARY KEY CLUSTERED (idNota ASC),
	CONSTRAINT [FK_notas_ToTable] FOREIGN KEY (idCategoria) REFERENCES categorias (idCategoria)
);